﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    Rigidbody rb;
    [SerializeField]Vector3 forward = new Vector3(x:1,y:0,z:0);
    [SerializeField]Vector3 initialPosition = new Vector3(-5,0,0);
    bool isMovingForward = false;
    bool toJump = false;
    bool canJump = false;
    [SerializeField]float maxSpeed = 5f;
    [SerializeField]Vector3 vel;
    [SerializeField]float timeToStop = 0.1f;
    [SerializeField]float jumpForce = 5;
    [SerializeField] Spawner spawner;
    [SerializeField] Spawner spawner1;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        Reset();
    }
	
	// Update is called once per frame
	public void Jump ()
    {
		//isMovingForward = Input.GetKey(KeyCode.RightArrow);
        if(canJump)
        {
            //toJump = true;
            Physics.gravity *= -1;
        }

	}

    private void OnCollisionEnter(Collision collision)
    {
        canJump = true;
        if(collision.gameObject.tag == "Enemy")
        {
            
            Reset();
            spawner.Rollback();
            spawner1.Rollback();
        }
    }

    private void Reset()
    {
        transform.position = initialPosition;
        rb.velocity = Vector3.zero;
        canJump = false;
        toJump = false;
    }

    private void OnCollisionExit(Collision collision)
    {
        canJump = false;
    }

    private void FixedUpdate()
    {
        
        if(isMovingForward)
        {
            rb.AddForce(forward);
        }

        if (toJump)
        {
            toJump = false;
            rb.AddForce(Vector3.up * jumpForce,ForceMode.Impulse);
        }

        vel = rb.velocity;
        vel.x = Mathf.Clamp(vel.x,0f,maxSpeed);
        if (!isMovingForward)
        {
            vel.x = Mathf.MoveTowards(vel.x,0,timeToStop);
        }
        rb.velocity = vel;

    }
}
