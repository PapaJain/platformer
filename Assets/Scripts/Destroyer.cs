﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    [SerializeField] Spawner spawner;

    private void OnTriggerEnter(Collider collision)
    {
        

        spawner.Add(collision.gameObject);
    }
}
