﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField] List<GameObject> spawnObjects = new List<GameObject>();
    [SerializeField] float spawnDelay = 2f, minTime = 1.2f, maxTime = 3f;
    
    void Start () {
        spawnDelay = Random.Range(minTime, maxTime);
	}
	
    float timer = 0;
	void Update () {
        timer += Time.deltaTime;
		while(timer >= spawnDelay)
        {
            timer -= spawnDelay;
            Spawn();
        }
	}

    public void Rollback()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform t = transform.GetChild(i);
            if (t.gameObject.activeSelf)
            {
                Add(t.gameObject);
            }
        }
        
    }

    void Spawn()
    {
        int randomIndex = Random.Range(0,spawnObjects.Count);
        GameObject g = spawnObjects[randomIndex];
        g.transform.position = transform.position;
        g.SetActive(true);
        spawnObjects.RemoveAt(randomIndex);
        spawnDelay = Random.Range(minTime, maxTime);
    }

    public void Add(GameObject gameobj)
    {
        gameobj.SetActive(false);
        spawnObjects.Add(gameobj);
    }

}
